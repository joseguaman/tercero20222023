/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista.utilidades;

import controlador.BolsaController;
import javax.swing.JComboBox;
import modelo.Tipo;

/**
 *
 * @author sebastian
 */
public class Utilidades {
    public static JComboBox cargarComboTipo(JComboBox cbx){
        cbx.removeAllItems();
        for(Tipo tipo: Tipo.values()) {
            cbx.addItem(tipo);
        }
        return cbx;
    }
}
